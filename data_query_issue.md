

Query Produced
[LINK](url)

Associated README
[LINK](url)

**I, the Data Analyst have:**<br>
- [ ] Written the appropriate query(ies)
- [ ] Linked the location of the query repository and README
- [ ] Updated the CI/CD to generate new merge sheet
- [ ] Validated content of README for details and acceptance criteria

**I, the Tester have:**<br>
- [ ] Validated that the query meets relevent acceptance criteria (if applicable)
- [ ] Confirmed that the new merge sheet meets agreed acceptance criteria.

**I, the Author have:**<br>
* [ ] Assigned this ticket to the appropriate EPIC
* [ ] Added any relevant labels
* [ ] Estimated the time needed to complete this task (weight = estimated hours)
* [ ] Assigned this ticket to the appropriate MILESTONE
* [ ] Linked any related gitlab issues
