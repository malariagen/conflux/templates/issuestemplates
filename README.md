# Issue Templates

In this repository you can find the source template for gitlab issues. If you wish to make a change are not the designated owner, you'll need merge approval from them.

| Template | Function | Owned by |
| ------ | ------ | ------ |
| auto_test.md | Define requirements and steps for creating an automated test. | Jyothy Kadi |
| bug.md | Used to raise and descibe a bug. | Jyothy Kadi |
| dev_issue.md | Describes a defined development task as well as implementation details. | Jon Keatley |
| user-story.md | Legacy template not currently in use | Chris Jacob |
| merge_request.md | Defines pre-requisites for applicable merge requests. | Kem Govender |

**CoTrack Issue Repo** - ensure updates happen here as well, until automation is possible.
https://gitlab.com/malariagen/conflux/c19/covid-design/-/tree/master/.gitlab/issue_templates

**Conflux Issue Repo** - ensure updates happen here as well, until automation is possible.
https://gitlab.com/malariagen/conflux/conflux/design/-/tree/master/.gitlab/issue_templates
