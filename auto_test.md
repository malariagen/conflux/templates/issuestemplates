This Automation task will contribute to Epic [ EPIC NAME ] with the specific aim to [ AIM ].

To accomplish this, the following steps must be taken.

Please before start automation check the Requirement/Acceptance criteria for the Feature need to discussed and captured (3 amigos)


- [ ] Step 1: Assign the ticket /Feature related to this task

- [ ] Step 2. Create an automation issue for each Automation Test case/Feature files - Gherkin format (BDD)

- [ ] Step 3: Split the task if necessary to front and back end automation

- [ ] Step 4 : The Test case that we have created should be captured  both in the Test case excel file 

- [ ] Step 5: Review the test case if we need to add it to  the End to end journey of the application


Gherkin Scenarios
Example: 

``` Scenario 1: Verify the user can log in to the account with valid credential 
    Given User is on the landing page 
    And the user can see the log in button 
    When user click on the login button 
    And enter valid username and password
    And click on log in 
    Then user is successfully log in to the account 
    

```

Automation Testing Ticket should be only closed 

Test cases/ Scenarios 
- [ ] After all the 5 steps are completed 
- [ ] Merge request is complete and merged to master of the Test repo
- [ ] Test passed/Failed Status screen shot is attached 
- [ ] Any bug that have been found as result of this test we need to create the bug and assign it to the relevant ticket 


- [ ] Assigned this ticket to the appropriate MILESTONE
- [ ] Linked any related gitlab issues
