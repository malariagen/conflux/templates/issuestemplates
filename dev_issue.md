## Development Team Group Guidance

1. [ fill ]
1. 

## Finished Work is defined as:

1. [ fill ]
1. 

We, the development team have:
- [ ] Assigned this ticket to the appropriate EPIC
- [ ] Added any relevant labels
- [ ] Estimated the time needed to complete this task (Fibonacci)
- [ ] Assigned this ticket to the appropriate MILESTONE
- [ ] Linked any related gitlab issues

